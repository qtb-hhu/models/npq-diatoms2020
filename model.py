#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:17:01 2020

Simple matematical model of NPQ in plants has been adapted for diatoms,
to test hypothesis regarding NPQ regulation and role of KEA3 antiporter

@author: Matuszynska
"""


import numpy as np
from modelbase.ode import Model
import matplotlib.pyplot as plt

plt.style.use('ggplot')

#Comments from original implementation by Matuszynska et al 2016

pars = {
            
    #Pool sizes 
    'PSIItot': 2.5,  # unchanged [mmol/molChl] total concentration of PSII        
    'PQtot': 20.,     # unchanged [mmol/molChl]      
    'APtot': 50.,     # unchanged [mmol/molChl] Bionumbers ~2.55mM (=81mmol/molChl)      
    'Lhcxtot': .5,    # changed [relative] refers now to available protein, scaling quencher     
    'Xtot': 1.,       # unchanged [relative] xanthophylls     
    'O2ex': 8.,       # unchanged external oxygen, kept constant, corresponds to 250 microM, corr. to 20%      
    'Pi': 0.01,      # unchanged       
    
    'kkea': 1./3,   # new partitioning of protons for a WT
            
    #Rate constants and key parameters
    'kCytb6f': 0.104,          # unchanged a rough estimate of the transfer from PQ to cyt that is equal to ~ 10ms
                               # [1/s*(mmol/(s*m^2))] - gets multiplied by light to determine rate
    'kActATPase': 0.01,        # unchangedparamter relating the rate constant of activation of the ATPase in the light
    'kDeactATPase': 0.002,     # unchangedparamter relating the deactivation of the ATPase at night
    'kATPsynthase': 20.,       # unchanged
    'kATPconsumption': 10.,    # unchanged
    'kPQred': 250.,            # unchanged [1/(s*(mmol/molChl))]
    'kH': 5e9,                 # unchanged
    'kF': 6.25e8,              # unchanged fluorescence 16ns
    'kP': 1.2e9,               # changed original 5e9 (charge separation limiting step ~ 200ps) - made this faster for higher Fs fluorescence
    'pHstroma': 7.8,           # unchanged [1/s] leakage rate
    'kleak': 1000.,            # unchanged
    'bH': 100,                 # unchanged proton buffer: ratio total / free protons
    'HPR': 14./3.,             # unchanged
            
    #Parameter associated with xanthophyll cycle
    'kDeepox': 0.025,          # changed, 
    'kEpox': 0.0096,           # changed, same range as kDeepox
    'KphSatZ': 6.3,            # changed based on ye experimental evidence
                               # [-] half-saturation pH value for activity de-epoxidase, highest activity at ~pH 5.8
    'nHX': 5.,                 # unchanged, the cooperativity, hill-coefficient for activity of de-epoxidase
    'ketc': 1.,                # factor to change the speed of epoxidise under limiting light conditions
            
            
    #Physical constants
    'F': 96.485,  # unchanged Faraday constant
    'R': 8.3e-3,  # unchanged universal gas constant
    'T': 298,     # unchanged Temperature in K - for now assumed to be constant at 25 C
            
    #Standard potentials and DG0ATP
    'E0QAQAm': -0.140,         # unchanged
    'E0PQPQH2': 0.354,         # unchanged
    'E0PCPCm': 0.350,          # changed for the DIATOMS
    'DG0ATP': 30.6,            # unchanged [kJ/mol / RT]
            
    #PFD
    'pfd': 0.                  # reference value set to 0.
            }

def calculate_pHstroma(x):
    return (-np.log(x*(3.2e-5))/np.log(10))    

def calculate_pHinv(x):
    ''' for diatoms'''
    return (3.125e4 * 10 **(-x))

def calculate_pH(H):
    "assumed bigger lumen by changing the volume"
    return -np.log10(H*3.2e-5)


# define the basic model 
model_diatom = Model(pars)

# add compounds 
model_diatom.add_compounds([
    "P", # reduced Plastoquinone
    "H", # luminal Protons
    "E", # ATPactivity
    "A", # ATP
    "Dd" # fraction of epoxidised xantohylls (Dd + Dt = Xtot)
    ])
    
model_diatom.add_derived_parameter(
    parameter_name="RT", function=lambda r, t: r * t, parameters=["R", "T"])

def _KeqQAPQ(F, E0QAQAm, E0PQPQH2, pHstroma, RT):
    DG1 =  -F*E0QAQAm 
    DG2 = -2*F*E0PQPQH2 + 2*pHstroma * np.log(10) * RT 
    DG0 = -2*DG1 + DG2
    Keq = np.exp(-DG0/RT)
    return Keq

model_diatom.add_derived_parameter(
    parameter_name = "KeqQAPQ", function=_KeqQAPQ, parameters=["F", "E0QAQAm", "E0PQPQH2", "pHstroma", "RT"])

def ps2states(P, Q, light, PQtot, kPQred, KeqQAPQ, kH, kF, kP, PSIItot):
    """Calculates the states of photosystem II
    
    accepts:
    P: reduced fraction of PQ pool (PQH2)
    Q: Quencher
    
    returns:
    B: array of PSII states
    """
    
    Bs = []
    Pox = PQtot - P
    b0 = (light + kPQred*P/KeqQAPQ)
    b1 = (kH * Q + kF)
    b2 = kH * Q + kF + kP
    
    for Pox,b0,b1,b2 in zip(Pox,b0,b1,b2):
        A = np.array([
        [-b0,        b1,         kPQred*Pox,                0], #B0
        [light,     -b2,         0,                         0], #B1
        [0,          0,          light,                   -b1], #B3
        [1,          1,          1,                         1]
        ])
        
        b = np.array([0,0,0,PSIItot])
        B0,B1,B2,B3 = np.linalg.solve(A,b)
        Bs.append([B0, B1, B2, B3])
    return np.array(Bs).T


def Keqcytb6f(H_beta, F, E0PQPQH2, RT, E0PCPCm, pHstroma):
    """Equilibriu constant of Cytochrome b6f"""
    DG1 = -2*F*E0PQPQH2 + 2 * RT * np.log(10) * calculate_pH(H_beta)
    DG2 = -F*E0PCPCm
    DG3 = RT*np.log(10)*(pHstroma - calculate_pH(H_beta))
    DG = -DG1 + 2*DG2 + 2*DG3
    Keq = np.exp(-DG/RT)
    return Keq
    
    
def KeqATPsyn(H, DG0ATP, pHstroma, RT, Pi):
    """Equilibrium constant of ATP synthase. For more
    information see Matuszynska et al 2016 or Ebenhöh et al. 2011,2014
    """
    DG = DG0ATP - np.log(10) * (pHstroma-calculate_pH(H)) * (14/3)  * RT
    Keq = Pi * np.exp(-DG/RT) 
    return Keq

def Fluorescence(P, Q, B0, B2, light, PQtot, kPQred, KeqQAPQ, kH, kF, kP, PSIItot):
    """Fluorescence function"""
    Fluo =  kF/(kH*Q + kF + kP) * B0 + kF/(kH*Q + kF) * B2
    return Fluo

def Quencher(Dd, Xtot, lhc):
    """Quencher mechanism
    
    accepts: 
    Pr: fraction of non-protonated PsbS protein
    V: fraction of Violaxanthin
    """
    Dt = Xtot - Dd
        
    Q = lhc * Dt
    return Q


def pqmoiety(P, PQtot):
    return [PQtot - P]

def atpmoiety(A, APtot):
    return [APtot - A]

def xcycmoiety(X, Xtot):
    return [Xtot - X]

def hbeta(H, kkea):
    return kkea * H

model_diatom.add_algebraic_module(
        module_name = 'Proton_beta',
        function = hbeta,
        compounds = ["H"],
        derived_compounds = ['H_beta'],
        parameters = ['kkea']
              )

model_diatom.add_algebraic_module(
    module_name="calculate_pH",
    function=calculate_pH,
    compounds=["H_beta"],
    derived_compounds=["pH"],
    modifiers=None
)


model_diatom.add_algebraic_module(
    module_name = "P_am",
    function = pqmoiety,
    compounds = ["P"],
    derived_compounds = ["Pox"],
    parameters = ["PQtot"])


model_diatom.add_algebraic_module(
    module_name = "A_am",
    function = atpmoiety,
    compounds = ["A"],
    derived_compounds = ["ADP"],
    parameters = ["APtot"])


model_diatom.add_algebraic_module(
    module_name = "X_am",
    function = xcycmoiety,
    compounds = ["Dd"],
    derived_compounds = ["Dt"],
    parameters = ["Xtot"])


model_diatom.add_algebraic_module(
    module_name = "Quencher",
    function = Quencher,
    compounds = ["Dd"],
    derived_compounds = ["Q"],
    parameters = ["Xtot", "Lhcxtot"])


model_diatom.add_algebraic_module(
    module_name = 'PSIIstates',
    function = ps2states,
    compounds = ["P", "Q"],
    derived_compounds = ["B0", "B1", "B2", "B3"],
    parameters = ["pfd", "PQtot", "kPQred",
                  "KeqQAPQ", "kH",  "kF", "kP", "PSIItot"])


model_diatom.add_algebraic_module(
    module_name = "Fluorescence",
    function = Fluorescence,
    compounds = ["P", "Q", "B0", "B2"],
    derived_compounds = ["Fluo"],
    parameters = ['pfd', 'PQtot', 'kPQred', 'KeqQAPQ',
                  'kH',  'kF', 'kP', 'PSIItot'])
    
    
model_diatom.add_algebraic_module(
    module_name = "Light",
    function = lambda X, PFD: PFD ,
    compounds = ["P"],
    derived_compounds = ["L"],
    parameters = ['pfd'])

#========= REACTION RATES ============#    

def vps2(B1, kP): 
    """Reduction of PQ due to ps2"""
    v = kP * 0.5 * B1
    return v  
   
model_diatom.add_reaction(
        rate_name = "vps2",
        function = vps2,
        modifiers=["B1"],
        stoichiometry = {"P":1,"H":2/pars["bH"]},
        parameters = ["kP"])


def vPQox(P, H_beta, light, kCytb6f, O2ex, PQtot, F, E0PQPQH2, RT, E0PCPCm, pHstroma):
    """Oxidation of the PQ pool through cytochrome and PTOX"""
    kPFD = kCytb6f * light
    Keq = Keqcytb6f(H_beta, F, E0PQPQH2, RT, E0PCPCm, pHstroma)
    a1 = kPFD * Keq/ (Keq + 1)  
    a2 = kPFD/(Keq + 1)
    v = a1 * P - a2 * (PQtot - P)
    return v

model_diatom.add_reaction(
        rate_name = "vPQox",
        function = vPQox,
        modifiers = ['H_beta'],
        stoichiometry = {"P":-1,"H":4/pars["bH"]},
        parameters = ["pfd", "kCytb6f", "O2ex",
                          "PQtot", "F", "E0PQPQH2", "RT", "E0PCPCm",
                          "pHstroma"])

  
def vATPactivity(E, light,  kActATPase,  kDeactATPase):
    """Activation of ATPsynthase by light"""
    switch = light > 0.
    v = kActATPase * switch * (1 - E) - kDeactATPase * (1-switch) * E
    return v

model_diatom.add_reaction(
        rate_name = "vATPactivity",
        function = vATPactivity,
        modifiers = ["E"],
        stoichiometry = {"E":1},
        parameters = ["pfd",  "kActATPase",  "kDeactATPase"])


def vATPsynthase(H, A, E, pfd, kATPsynthase, DG0ATP, pHstroma, RT, Pi, APtot):
    """Production of ATP by ATPsynthase"""
    v = E * kATPsynthase * (APtot - A - A/KeqATPsyn(H, DG0ATP, pHstroma, RT, Pi))
    return v
    
model_diatom.add_reaction(
        rate_name = "vATPsynthase",
        function = vATPsynthase,
        modifiers = ["A","E"],
        stoichiometry = {"A":1, "H": (-14/3)/pars["bH"]},
        parameters = ["pfd", "kATPsynthase", "DG0ATP", "pHstroma",
                      "RT", "Pi", "APtot"])
    

def vATPcons(A, kATPconsumption):
    """ATP consuming reaction"""
    v = kATPconsumption * A
    return v

model_diatom.add_reaction(
        rate_name = "vATPcons",
        function = vATPcons,
        stoichiometry = {"A":-1},
        parameters = ["kATPconsumption"])


def vLeak(H, kleak, pHstroma):
    """Transmembrane proton leak"""
    v = kleak * (H - calculate_pHinv(pHstroma))
    return v

model_diatom.add_reaction(
        rate_name = "vKEA3",
        function  = vLeak,
        stoichiometry = {"H": -1/pars["bH"]},
        parameters = ["kleak", "pHstroma"])


def kd(H, nHX, KphSatZ, kDeepoxV):
    """Deepoxidation of Dd"""
    a = H**nHX / (H**nHX + calculate_pHinv(KphSatZ)**nHX)
    v = kDeepoxV * a
    return v

model_diatom.add_algebraic_module(
    module_name="kd",
    function=kd,
    compounds=["H_beta"],
    derived_compounds=["kd"],
    parameters=["nHX", "KphSatZ", "kDeepox"]
)

def vXdeepox(X, kd):
    """Deepoxidation of Dd"""
    v = kd * X
    return v

model_diatom.add_reaction(
        rate_name = "vXdeepox",
        function = vXdeepox,
        modifiers = ['kd'],
        stoichiometry = {"Dd": -1},
        parameters = None)

def ke(X, kd, Xtot):
    """Expoxidation of Dt"""
    return kd*X/(Xtot-X)
    
model_diatom.add_algebraic_module(
    module_name="ke",
    function=ke,
    compounds=["Dd", "kd"],
    derived_compounds=["ke"],
    parameters=["Xtot"]
)

   
def vXepox(L, Dt, ke, ketc):
    """Expoxidation of Dt"""
    for l in L:
        if l<=50.:
            return  ketc * ke * Dt    
        else:
            return ke * Dt  
    
    
model_diatom.add_reaction(
        rate_name = "vXepox",
        function = vXepox,
        modifiers = ['L','Dt'],
        stoichiometry = {"Dd": 1},
        parameters = ["kEpox", "ketc"])